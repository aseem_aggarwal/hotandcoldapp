//
//  DistanceDetectorAppDelegate.h
//  DistanceDetectorApp
//
//  Created by Aseem Aggarwal on 2/25/14.
//  Copyright (c) 2014 Aseem Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistanceDetectorAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
