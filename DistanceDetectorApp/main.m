//
//  main.m
//  DistanceDetectorApp
//
//  Created by Aseem Aggarwal on 2/25/14.
//  Copyright (c) 2014 Aseem Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DistanceDetectorAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DistanceDetectorAppDelegate class]));
    }
}
